<?php
	use App\ApplicationKernel;
	require_once __DIR__.'/app/bootstrap.php.cache';
	$kernel = new ApplicationKernel('dev', true);
	$kernel->boot();
	$entityManager = $kernel->getContainer()->get('entityManager');
	return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);