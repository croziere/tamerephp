<?php
	namespace Tamere\Event;

	class Event {
		
		private $toStop = false;

		public function stop() {
			$this->toStop = true;
		}

		public function isStopped() {
			return $this->toStop;
		}
	}