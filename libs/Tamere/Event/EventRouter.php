<?php
	namespace Tamere\Event;

	class EventRouter implements EventRouterInterface {

		private $observers = array();

		public function route($eventName, Event $event = null) {
			if(null === $event) {
				$event = new Event();
			}

			if($this->hasObservers($eventName)) {
				$this->notify($this->getObservers($eventName), $eventName, $event);
			}

			return $event;
		}

		public function addObserver($eventName, $callback) {
			$this->observers[$eventName][] = $callback;
		}

		public function removeObserver($eventName, $callback) {
			if(!isset($this->observers[$eventName])){
				return;
			}

			foreach ($this->observers as $key => $cb) {
				if($cb === $callback){
					unset($this->observers[$eventName][$key]);
				}
			}
		}

		public function registerObservers(EventObserverInterface $observers) {
			foreach ($observers->getObservedEvents() as $eventName => $params) {
				if(is_string($params)){
					$this->addObserver($eventName, array($observers, $params));
				}
				else {
					foreach ($params as $cb) {
						$this->addObserver($eventName, array($observers, $cb));
					}
				}
			}
		}

		public function unregisterObservers(EventObserverInterface $observers) {
			throw new Exception("unregisterObservers is not implemented !@todo");			
		}

		public function getObservers($eventName) {
			if(!isset($this->observers[$eventName])) {
				return array();
			}

			return $this->observers[$eventName];
		}

		public function hasObservers($eventName) {
			return (bool) count($this->getObservers($eventName));
		}

		protected function notify($observers, $eventName, Event $event) {
			foreach ($observers as $observer) {
				call_user_func($observer, $event, $eventName, $this);
				if($event->isStopped()){
					break;
				}
			}
		}

	}