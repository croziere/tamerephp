<?php
	namespace Tamere\Event;

	interface EventObserverInterface {

		public function getObservedEvents();

	}