<?php
	namespace Tamere\Event;

	interface EventRouterInterface {
		
		public function route($eventName, Event $event = null);

		public function addObserver($eventName, $callback);

		public function removeObserver($eventName, $callback);

		public function registerObservers(EventObserverInterface $observers);

		public function unregisterObservers(EventObserverInterface $observers);

		public function getObservers($eventName);

		public function hasObservers($eventName);
	}