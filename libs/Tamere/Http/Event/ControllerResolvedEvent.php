<?php
	namespace Tamere\Http\Event;

	use Tamere\Http\Request;
	use Tamere\Http\HttpProcessorInterface;

	class ControllerResolvedEvent extends CoreEvent {

		private $controller;

		public function __construct(Request $request, HttpProcessorInterface $processor, $controller){
			parent::__construct($request, $processor);
			$this->controller = $controller;
		}

		public function setController($controller)
		{
			$this->controller = $controller;
			$this->stop();
		}

		public function getController(){
			return $this->controller;
		}
	}