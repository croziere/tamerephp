<?php
	namespace Tamere\Http\Event;

	use Tamere\Http\Request;
	use Tamere\Http\HttpProcessorInterface;

	class GetResponseFromResultEvent extends GetResponseEvent {

		private $raw;

		public function __construct(Request $request, HttpProcessorInterface $processor, $raw){
			parent::__construct($request, $processor);
			$this->raw = $raw;
		}

		public function getRaw(){
			return $this->raw;
		}

	}