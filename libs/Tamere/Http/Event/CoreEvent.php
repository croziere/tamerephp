<?php
	namespace Tamere\Http\Event;

	use Tamere\Http\Request;
	use Tamere\Http\HttpProcessorInterface;
	use Tamere\Event\Event;

	class CoreEvent extends Event {

		private $request;

		private $processor;

		public function __construct(Request $request, HttpProcessorInterface $processor) {
			$this->processor = $processor;
			$this->request = $request;
		}

		public function getProcessor() {
			return $this->processor;
		}

		public function getRequest() {
			return $this->request;
		}

	}