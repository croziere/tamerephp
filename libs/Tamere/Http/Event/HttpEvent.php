<?php
	namespace Tamere\Http\Event;

	final class HttpEvent {

		const REQUEST = 'core.request'; //Arrivée de la requete

		const RESOLVER = 'core.resolve'; //Handler final

		const CONTROLLER = 'core.controller'; //Execution du handler

		const RESPONSE = 'core.response'; //Requete du handler recue

		const TERMINATE = 'core.terminate'; //Fin d'execution du processor

		const EXCEPTION = 'core.exception'; //Erreur lors du traitement

	}