<?php
	namespace Tamere\Http\Event;

	use Tamere\Http\Response;

	class GetResponseEvent extends CoreEvent {

		protected $response;

		public function getResponse() {
			return $this->response;
		}

		public function setResponse(Response $response) {
			$this->response = $response;

			$this->stop();
		}

		public function hasResponse() {
			return null !== $this->response;
		}

	}