<?php
	namespace Tamere\Http\Event;

	use Tamere\Http\Request;
	use Tamere\Http\Response;
	use Tamere\Http\HttpProcessorInterface;

	class BeforeSendResponseEvent extends CoreEvent {

		private $response;

		public function __construct(Request $request, HttpProcessorInterface $processor, Response $response){
			parent::__construct($request, $processor);
			$this->setResponse($response);
		}

		public function setResponse(Response $response) {
			$this->response = $response;
		}

		public function getResponse() {
			return $this->response;
		}
	}