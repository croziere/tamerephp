<?php
	namespace Tamere\Http\Controller;

	use Tamere\Http\Request;

	interface ControllerResolverInterface {

		public function getController(Request $request);

		public function getArguments(Request $request, $controller);
	}