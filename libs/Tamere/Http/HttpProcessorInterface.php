<?php
	namespace Tamere\Http;

	use Tamere\Http\Request;

	interface HttpProcessorInterface {

		public function handle(Request $request);
	}