<?php

	namespace Tamere\Http;

	class ParametersBag {

		protected $parameters;

		public function __construct(array $parameters = array()) {
			$this->parameters = $parameters;
		}

		public function get($key) {
			return $this->parameters[$key];
		}

		public function has($key){
			return isset($this->parameters[$key]);
		}

		public function add($key, $value){
			$this->parameters[$key] = $value;
		}

		public function all(){
			return $this->parameters;
		}

		public function merge(array $params){
			$this->parameters = array_merge($this->parameters, $params);
		}
	}