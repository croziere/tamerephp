<?php
	namespace Tamere\Http\Modules;

	use Tamere\DependencyInjection\ContainerAware;
	use Tamere\DependencyInjection\ContainerRegisterInterface;

	abstract class Module extends ContainerAware implements ModuleInterface {

		protected $name;
		protected $containerRegister;
		protected $path;


		public function boot() {

		}

		public function getContainerRegister() {
			if(null === $this->containerRegister) {
				$class = $this->getContainerRegisterClass();

				if(class_exists($class)){
					$containerRegister = new $class();

					if(!$containerRegister instanceof ContainerRegisterInterface){
						throw new \LogicException(sprintf("La classe %s doit implémenter Tamere\DependencyInjection\ContainerRegisterInterface", $class));
					}
					$this->containerRegister = $containerRegister;
				}
				else
				{
					$this->containerRegister = false;
				}

			}

			if($this->containerRegister) {
				return $this->containerRegister;
			}
		}

		public function getContainerRegisterClass() {
			$name = preg_replace('/Module$/', '', $this->getName());
			return $this->getNamespace().'\\ContainerServices\\'.$name.'ContainerRegister';
		}

		public final function getName() {
			if (null !== $this->name) {
            	return $this->name;
        	}

        	$name = get_class($this);
        	$pos = strrpos($name, '\\');

        	return $this->name = false === $pos ? $name : substr($name, $pos + 1);
		}

		public final function getNamespace() {

			$ns = get_class($this);

			return substr($ns, 0, strrpos($ns, '\\'));
		}

		public final function getPath(){
			if(null === $this->path){
				$r = new \ReflectionObject($this);
				$this->path = dirname($r->getFileName());
			}
			return $this->path;
		}
	}