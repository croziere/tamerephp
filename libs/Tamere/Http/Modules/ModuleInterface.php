<?php
	namespace Tamere\Http\Modules;

	use Tamere\DependencyInjection\ContainerAwareInterface;

	interface ModuleInterface extends ContainerAwareInterface {

		public function boot();

		public function getName();

	}