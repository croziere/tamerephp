<?php
	namespace Tamere\Http;

	use Tamere\Http\ParametersBag;

	class Request {

		public $attributes; //Custom attrs

		public $request; //$_POST

		public $query; //$_GET

		public $server; //$_SERVER

		public $files; //$_FILES

		public $cookies; //$_COOKIE

		public $headers; //Headers in $_SERVER

		public $session; //$_SESSION

		private $uri;

		private $method;

		public function __construct($uri, $method, $request, $query, $server, $cookies, $files, $session = array()) {
			$this->attributes = new ParametersBag();
			$this->uri = $uri;
			$this->method = $method;
			
			$this->request = new ParametersBag($request);
			$this->query = new ParametersBag($query);
			$this->server = new ParametersBag($server);
			$this->cookies = new ParametersBag($cookies);
			$this->files = new ParametersBag($files);
			$this->session = new ParametersBag($session);

		}

		public static function createUserRequest() {

			$uri = $_SERVER['REQUEST_URI'];
			$script = $_SERVER['SCRIPT_NAME'];
			$uri = str_replace($script, '', $uri);
			if(strlen($uri) === 0)
				$uri = '/';
			session_start();
			return new static($uri, $_SERVER['REQUEST_METHOD'], $_POST, $_GET, $_SERVER, $_COOKIE, $_FILES, $_SESSION);
		}

		public function getPath(){
			return $this->uri;
		}
	}