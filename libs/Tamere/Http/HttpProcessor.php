<?php
	namespace Tamere\Http;

	use Tamere\Http\HttpProcessorInterface;
	use Tamere\Http\Request;
	use Tamere\Http\Response;
	use Tamere\Event\EventRouterInterface;
	use Tamere\Http\Event\HttpEvent;
	use Tamere\Http\Event\GetResponseEvent;
	use Tamere\Http\Event\BeforeSendResponseEvent;
	use Tamere\Http\Event\ControllerResolvedEvent;
	use Tamere\Http\Event\GetResponseFromResultEvent;
	use Tamere\Http\Controller\ControllerResolverInterface;

	class HttpProcessor implements HttpProcessorInterface {

		private $eventRouter;

		private $resolver;

		public function __construct(EventRouterInterface $eventRouter, ControllerResolverInterface $resolver) {
			$this->eventRouter = $eventRouter;
			$this->resolver = $resolver;		
		}

		public function handle(Request $request) {

			try{
				return $this->handleUserRequest($request);
			}
			catch(\Exception $e){
				return $this->handleException($e, $request);
			}

		}

		private function handleUserRequest(Request $request){

			//1. Auth, changement de request, debug, etc...
			$event = new GetResponseEvent($request, $this);
			$this->eventRouter->route(HttpEvent::REQUEST, $event);

			if($event->hasResponse()){
				return $this->beforeSend($event->getResponse(), $request);
			}
			
			$controller = $this->resolver->getController($request);
			if(!$controller){
				throw new \Exception('Controller not found !');
			}

			$event = new ControllerResolvedEvent($request, $this, $controller);
			$this->eventRouter->route(HttpEvent::CONTROLLER, $event);
			$controller = $event->getController();

			$arguments = $this->resolver->getArguments($request, $controller);

			$response = call_user_func_array($controller, $arguments);

			if(!$response instanceof Response){

				$event = new GetResponseFromResultEvent($request, $this, $response);
				$this->eventRouter->route(HttpEvent::RESPONSE, $event);

				if($event->hasResponse()){
					$response = $event->getResponse();
				}

				if(!$response instanceof Response){
					$err = sprintf('Le controller doit renvoyer une instance de Response (%s retourné)', $this->dump_type($response));
					if(null === $response){
						$err .= ' Return a surement été oublié dans le controlleur !';
					}
					throw new \LogicException($err);
				}
			}

			return $this->beforeSend($response, $request);
		}

		private function handleException($e, Request $request){
			$response = new Response();
			$response->setContent("Oops : ".$e);
			return $response;
		}

		private function beforeSend(Response $response, Request $request) {
			$event = new BeforeSendResponseEvent($request, $this, $response);

			$this->eventRouter->route(HttpEvent::TERMINATE);

			return $event->getResponse();
		}

		private function dump_type($var){
			if (is_object($var)) {
            return sprintf('Object(%s)', get_class($var));
        	}

	        if (is_array($var)) {
	            $a = array();
	            foreach ($var as $k => $v) {
	                $a[] = sprintf('%s => %s', $k, $this->varToString($v));
	            }

	            return sprintf('Array(%s)', implode(', ', $a));
	        }

	        if (is_resource($var)) {
	            return sprintf('Resource(%s)', get_resource_type($var));
	        }

	        if (null === $var) {
	            return 'null';
	        }

	        if (false === $var) {
	            return 'false';
	        }

	        if (true === $var) {
	            return 'true';
	        }

	        return (string) $var;
		}

	}