<?php
	namespace Tamere\Modules\DoctrineModule;

	use Tamere\Http\Modules\Module;
	use Tamere\Modules\ConfigModule\EventObservers\TestRequestObserver;
	use Doctrine\ORM\Tools\Setup;
	use Doctrine\ORM\EntityManager;

	class DoctrineModule extends Module {

		public function boot() {
			$kernel = $this->container->get('kernel');
			$modules = $kernel->getModules();
			$paths = array();
			foreach($modules as $module){
				if(true === is_dir($module->getPath().'/Entity')){
					$paths[] = $module->getPath().'/Entity';
				}
			}
			$config = Setup::createAnnotationMetadataConfiguration($paths, true);
			
			$conn = array(
				'driver' => 'pdo_mysql',
				'dbname' => 'tamere',
				'user' => 'tamere',
				'password' => 'pass',
				'host' => 'localhost');
			$entityManager = EntityManager::create($conn, $config);
			$this->container->set('entityManager', $entityManager);
			$this->initFirewall($entityManager);
		}

		private function initFirewall($em){
			$authenticators = array(
				new \Tamere\Security\Authenticator\LogoutAuthenticator(),
				new \Tamere\Security\Authenticator\FormAuthenticator($em->getRepository('Blog\UserModule\Entity\User')),
				new \Tamere\Security\Authenticator\SessionAuthenticator($em->getRepository('Blog\UserModule\Entity\User')),
				new \Tamere\Security\Authenticator\AnonymousAuthenticator()
				);

			$firewall = new \Tamere\Security\Firewall($authenticators, $this->container->get('templating'));
			$this->container->set('firewall', $firewall); 
			$eventRt = $this->container->get('event_router');
			$eventRt->registerObservers($firewall);
		}
	}