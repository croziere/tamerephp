<?php

	namespace Tamere\Modules\ConfigModule\ContainerServices;

	use Tamere\DependencyInjection\ContainerRegisterInterface;
	use Tamere\DependencyInjection\Injectable\Definition;
	use Tamere\DependencyInjection\Injectable\Reference;
	use Tamere\DependencyInjection\ContainerInterface;
	use Tamere\Http\HttpProcessor;
	use Tamere\Event\EventRouter;
	use Tamere\Routing\Router;

	class ConfigContainerRegister implements ContainerRegisterInterface {

		public function registerExtensions(ContainerInterface $container) {
			$httpProcessorDef = new Definition('Tamere\Http\HttpProcessor');
			$httpProcessorDef->addArgument(new Reference('event_router'))
							 ->addArgument(new Reference('controller_resolver'));

			$resolverDef = new Definition('Tamere\Http\Controller\ControllerResolver');

			$routingObsDef = new Definition('Tamere\Modules\ConfigModule\EventObservers\RoutingObserver');
			$routingObsDef->addArgument(new Reference('router'));

			$controllerInjectorDef = new Definition('Tamere\Modules\ConfigModule\EventObservers\ControllerInjectorObserver');
			$controllerInjectorDef->addArgument(new Reference('container'));

			$tplLoaderDef = $this->createLoaderDef($container);

			$tplParsDef = new Definition('Symfony\Component\Templating\TemplateNameParser');

			$tplSlotDef = new Definition('Symfony\Component\Templating\Helper\SlotsHelper');

			$tplEngine = new Definition('Symfony\Component\Templating\PhpEngine');
			$tplEngine->addArgument(new Reference('template.parser'));
			$tplEngine->addArgument(new Reference('template.loader'));
			$tplEngine->addCall('set', array(new Reference('template.slots')));

			$container->register('controller.injector', $controllerInjectorDef);
			$container->register('routing.observer', $routingObsDef);
			$container->register('template.loader', $tplLoaderDef);
			$container->register('template.parser', $tplParsDef);
			$container->register('template.slots', $tplSlotDef);
			$container->register('templating', $tplEngine);
			$container->set('router', new Router());
			$container->register('http_processor', $httpProcessorDef);
			$container->register('controller_resolver', $resolverDef);
			$container->set('event_router', new EventRouter());
			
		}

		private function createLoaderDef($container){
			$kernel = $container->get('kernel');
			$modules = $kernel->getModules();
			$paths = array();
			foreach($modules as $module){
				$paths[] = $module->getPath().'/Template/%name%';
			}
			$paths[] = $kernel->getAppRootDir().'/Template/%name%';

			$loaderDef = new Definition('Symfony\Component\Templating\Loader\FilesystemLoader');
			$loaderDef->addArgument($paths);
			return $loaderDef;
		}
	}