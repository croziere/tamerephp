<?php
	namespace Tamere\Modules\ConfigModule\EventObservers;

	use Tamere\Event\EventObserverInterface;
	use Tamere\Routing\RouterInterface;
	use Tamere\Http\Event\GetResponseEvent;

	class RoutingObserver implements EventObserverInterface{

		private $router;

		public function __construct(RouterInterface $router){
			$this->router = $router;
		}

		public function onRequest(GetResponseEvent $event, $eventName, $router){
			$request = $event->getRequest();
			if($request->attributes->has('_controller')){
				return;
			}

			$params = $this->router->match($request->getPath());
			if(false === $params){
				throw new \Exception(sprintf("Pas de route pour %s", $request->getPath()));
			}
			$request->attributes->merge($params);
		}


		public function getObservedEvents() {
			return array("core.request" => "onRequest");
		}
	}