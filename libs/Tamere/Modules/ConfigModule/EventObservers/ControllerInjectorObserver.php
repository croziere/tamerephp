<?php
	namespace Tamere\Modules\ConfigModule\EventObservers;

	use Tamere\Event\EventObserverInterface;
	use Tamere\Http\Event\ControllerResolvedEvent;
	use Tamere\Http\Event\HttpEvent;
	use Tamere\DependencyInjection\ContainerInterface;

	class ControllerInjectorObserver implements EventObserverInterface{

		private $container;

		public function __construct(ContainerInterface $container){
			$this->container = $container;
		}

		public function onResolved(ControllerResolvedEvent $event, $eventName, $router){
			$controller = $event->getController();
			if(!is_array($controller)){
				return;
			}

			if(get_parent_class($controller[0]) !== 'Tamere\DependencyInjection\ContainerAware'){
				return;
			}
			$ctlr = new $controller[0];
			$ctlr->setContainer($this->container);
			$controller = array($ctlr, $controller[1]);
			$event->setController($controller);
		}


		public function getObservedEvents() {
			return array(HttpEvent::CONTROLLER => "onResolved");
		}
	}