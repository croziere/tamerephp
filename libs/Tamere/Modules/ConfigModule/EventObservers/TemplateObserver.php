<?php
	namespace Tamere\Modules\ConfigModule\EventObservers;

	use Tamere\Event\EventObserverInterface;
	use Tamere\Http\Event\GetResponseFromResultEvent;
	use Tamere\Http\Response;
	use Tamere\Http\Event\HttpEvent;

	class TemplateObserver implements EventObserverInterface {


		public function getObservedEvents() {
			return array(HttpEvent::RESPONSE => "onRawResponse");
		}

		public function onRawResponse(GetResponseFromResultEvent $event, $eventName, $router) {
			if(!is_string($event->getRaw())){
				return;
			}
			$response = new Response();
			$response->setContent($event->getRaw());
			$event->setResponse($response);
		}
	}