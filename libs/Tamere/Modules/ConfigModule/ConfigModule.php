<?php
	namespace Tamere\Modules\ConfigModule;

	use Tamere\Http\Modules\Module;
	use Tamere\Modules\ConfigModule\EventObservers\TemplateObserver;

	class ConfigModule extends Module {

		public function boot() {
			$routingObs = $this->container->get('routing.observer');
			$ctlrObs = $this->container->get('controller.injector');
			$eventRt = $this->container->get('event_router');
			$eventRt->registerObservers($routingObs);
			$eventRt->registerObservers($ctlrObs);
			$eventRt->registerObservers(new TemplateObserver());
		}
	}