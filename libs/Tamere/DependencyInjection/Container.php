<?php
	namespace Tamere\DependencyInjection;

	use Tamere\DependencyInjection\ContainerInterface;
	use Tamere\DependencyInjection\Exception\ServiceNotFoundException;

	class Container implements ContainerInterface {

		protected $services = array();

		public function set($id, $service) {

			$id = strtolower($id);

			if('container' === $id){
				return;
			}

			if(null === $service){
				if(isset($this->services[$id])){
					unset($this->services[$id]);
				}			
				return;
			}

			$this->services[$id] = $service;
		}

		public function get($id) {
			$id = strtolower($id);
			if('container' === $id)
				return $this;

			if(!isset($this->services[$id])){
				throw new ServiceNotFoundException(sprintf("Service %s inconnu", $id));				
			}

			return $this->services[$id];
		}

		public function has($id) {
			$id = strtolower($id);
			if('container' === $id || isset($this->services[$id])){
				return true;
			}

			return false;
		}

	}