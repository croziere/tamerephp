<?php
	namespace Tamere\DependencyInjection;

	use Tamere\DependencyInjection\ContainerInterface;

	interface ContainerAwareInterface {

		public function setContainer(ContainerInterface $container);
	}