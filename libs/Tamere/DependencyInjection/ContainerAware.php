<?php
	namespace Tamere\DependencyInjection;

	use Tamere\DependencyInjection\ContainerAwareInterface;
	use Tamere\DependencyInjection\ContainerInterface;

	abstract class ContainerAware implements ContainerAwareInterface {
		protected $container;

		public function setContainer(ContainerInterface $container) {
			$this->container = $container;
		}
	}