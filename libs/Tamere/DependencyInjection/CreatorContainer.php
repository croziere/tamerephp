<?php
	namespace Tamere\DependencyInjection;

	use Tamere\DependencyInjection\Injectable\Definition;
	use Tamere\DependencyInjection\Injectable\Reference;

	class CreatorContainer extends Container {

		private $registers = array();
		private $definitions = array();

		public function get($id) {
			$id = strtolower($id);

			try {
				$service = parent::get($id);				
				return $service;
			}
			catch(\Exception $e) {
			}
			

			if(!array_key_exists($id, $this->definitions)){
				throw new \LogicException(sprintf("La définition du service %s n'existe pas", $id));				
			}

			$service = $this->createService($this->definitions[$id], $id);

			return $service;

		}

		public function set($id, $service) {
			$id = strtolower($id);

			if($service instanceof Definition){
				$this->addDefinition($id, $service);
				return;
			}

			if(isset($this->definitions[$id])) {
				unset($this->definitions[$id]);
			}
			parent::set($id, $service);
		}

		private function createService(Definition $definition, $id) {
			$arguments = $this->resolveDependencies($definition->getArguments());

			$r = new \ReflectionClass($definition->getClass());
			$service = null === $r->getConstructor() ? $r->newInstance() : $r->newInstanceArgs($arguments);

			foreach ($definition->getMethodCalls() as $call) {
				$this->callMethod($service, $call);
			}

			return $service;
		}

		private function resolveDependencies($value) {
			if(is_array($value)){
				foreach($value as $k => $v) {
					$value[$k] = $this->resolveDependencies($v);
				}
			}
			elseif($value instanceof Reference) {
				$value = $this->get((string) $value);
			}
			else {
				$value = $value;
			}

			return $value;
		}

		private function callMethod($service, $callable){

			call_user_func_array(array($service, $callable[0]), $this->resolveDependencies($callable[1]));
		}

		public function register($id, Definition $definition) {
			$id = strtolower($id);

			return $this->definitions[$id] = $definition;
		}

		public function addRegister(ContainerRegisterInterface $extension) {
			$this->registers[] = $extension;
		}

		public function build() {
			foreach($this->registers as $r) {
				$r->registerExtensions($this);
			}		
		}
	}