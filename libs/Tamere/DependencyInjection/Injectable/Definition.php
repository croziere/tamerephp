<?php
	namespace Tamere\DependencyInjection\Injectable;

	class Definition {

		private $class;
		private $arguments = array();
		private $methodsCalls = array();

		public function __construct($class = null, $arguments = array()){
			$this->class = $class;
			$this->arguments = $arguments;
		}

		public function setClass($class) {
			$this->class = $class;
			return $this;
		}

		public function getClass() {
			return $this->class;
		}

		public function addArgument($argument) {
			$this->arguments[] = $argument;
			return $this;
		}

		public function getArguments(){
			return $this->arguments;
		}

		public function addCall($method, array $arguments = array()){
			$this->methodsCalls[] = array($method, $arguments);
			return $this;
		}

		public function getMethodCalls(){
			return $this->methodsCalls;
		}

	}