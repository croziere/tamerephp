<?php
	namespace Tamere\DependencyInjection\Injectable;

	class Reference {

		private $id;

		public function __construct($id) {
			$this->id = strtolower($id);
		}

		public function __toString() {
			return $this->id;
		}
	}