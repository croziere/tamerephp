<?php
	namespace Tamere\DependencyInjection;

	interface ContainerInterface {

		public function set($id, $service);

		public function get($id);

		public function has($id);

	}