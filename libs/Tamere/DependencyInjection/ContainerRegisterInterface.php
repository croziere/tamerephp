<?php
	namespace Tamere\DependencyInjection;

	interface ContainerRegisterInterface {

		public function registerExtensions(ContainerInterface $container);
	}