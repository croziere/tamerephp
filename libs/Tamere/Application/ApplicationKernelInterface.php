<?php
	namespace Tamere\Application;
	
	interface ApplicationKernelInterface {

		public function registerModules();

		public function getModules();

		public function getModule($name);

		public function boot();

		public function shutdown();

		public function getEnvironment();

		public function isDebug();

		public function getAppRootDir();

		public function getCacheDir();

		public function registerContainerConfig();

		public function getContainer();

}