<?php
	namespace Tamere\Application;

	use Tamere\Http\Request;
	use Tamere\Http\Response;
	use Tamere\Application\ApplicationKernelInterface;
	use Tamere\DependencyInjection\CreatorContainer as Container;
	use Tamere\Http\HttpProcessor;


	abstract class AbstractApplicationKernel implements ApplicationKernelInterface {

		protected $modules = array();

		protected $rootDir;
		protected $cacheDir;
		protected $debug;
		protected $container;

		protected $booted = false;

		public function __construct($environment, $debug) {
			$this->environment = $environment;
			$this->debug = (bool) $debug;
			$this->rootDir = $this->getAppRootDir();
		}

		public abstract function registerModules();

		public function getModules() {
			return $this->modules;
		}

		public function getModule($name) {
			if(!isset($this->modules[$name])) {
				return;
			}
			return $this->modules[$name];
		}

		public function initModules() {
			foreach($this->registerModules() as $module) {
				$name = $module->getName();

				if(isset($this->modules[$name])){
					throw new \LogicException(sprintf("Chargement de deux modules avec un nom identique : %s", $name));
				}

				$this->modules[$name] = $module;
			}
		}

		public function initContainer() {
			$this->container = new Container();
			$this->container->set('kernel', $this);

			foreach ($this->modules as $module) {
				if($module->getContainerRegister()) {
					$this->container->addRegister($module->getContainerRegister());
				}	
			}	

			$this->container->build();
		}

		public function boot() {
			if(true === $this->booted) {
				return;
			}

			$this->initModules();
			$this->initContainer();

			//Load modules
			foreach ($this->getModules() as $module) {
				$module->setContainer($this->container);
				$module->boot();
			}

			$this->booted = true;
		}

		public function shutdown() {

		}

		public function getEnvironment() {
			return $this->environment;
		}

		public function isDebug() {
			return $this->debug;
		}

		public function getAppRootDir() {

			if (null === $this->rootDir) {
            	$r = new \ReflectionObject($this);
            	$this->rootDir = dirname($r->getFileName());
        	}

        	return $this->rootDir;
		}

		public function getCacheDir() {
			return $this->rootDir."/cache/";
		}

		public function registerContainerConfig() {

		}

		public function getContainer() {
			return $this->container;
		}

		public function handle(Request $request) {
			if(false === $this->booted) {
				$this->boot();
			}
			return $this->getHttpProcessor()->handle($request);
		}

		public function getHttpProcessor() {
			return $this->container->get('http_processor');
		}
	}