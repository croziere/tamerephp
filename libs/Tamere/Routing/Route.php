<?php
	namespace Tamere\Routing;

	class Route {

		//De la forme '/articles/:id'
		private $path = '/';

		//':id' => '\d' (REGEX)
		private $attributes = array();

		private $compiled = null;

		private $methods = array();

		private $controller;

		public function __construct($path = '/', $attributes = array(), $methods = array(), $controller = null){

			$this->setPath($path);
			$this->attributes = $attributes;
			$this->methods = $methods;
			$this->controller = $controller;
			$this->compile();
		}

		public function setPath($path){
			$this->path = '/'.ltrim(trim($path), '/');
			$this->compiled = null;

			return $this;
		}

		public function getPath() {
			return $this->path;
		}

		public function setController($controller){
			$this->controller = $controller;
			return $this;
		}

		public function getController(){
			return $this->controller;
		}

		private function compile(){
			if(null !== $this->compiled){
				return;
			}
			$regex = '/^'.str_replace('/', '\/', $this->path).'$/';
			foreach ($this->attributes as $attr => $reg) {
				$fnd = '(['.$reg.']+)';
				$regex = str_replace(':'.$attr, $fnd, $regex);
			}
			$this->compiled = $regex;
		}

		public function getRegex() {
			$this->compile();
			return $this->compiled;
		}
	}