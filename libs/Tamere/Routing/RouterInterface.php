<?php

	namespace Tamere\Routing;

	interface RouterInterface {

		public function addRoute($id, Route $route);

		public function getUrl($id, $params);

		public function match($match);

		public function getController($id);

	}