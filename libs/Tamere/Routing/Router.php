<?php
	namespace Tamere\Routing;

	class Router implements RouterInterface {

		protected $routes = array();

		public function addRoute($id, Route $route) {
			$id = strtolower($id);

			$this->routes[$id] = $route;
		}

		public function getUrl($id, $params) {
			throw new \Exception("Not implemented !");
		}

		public function match($match) {
			$matches = array();
			foreach($this->routes as $id => $route){
				if(1 === preg_match($route->getRegex(), $match, $matches)){
					return $this->matchParameters($route, $matches);
				}
			}
			return false;
		}

		private function matchParameters(Route $route, array $matches){
			$paramsKey = array();
			preg_match_all('/:([[:alnum:]]+)\/?/', $route->getPath(), $paramsKey);
			$paramsKey = $paramsKey[1];
			unset($matches[0]);
			$params = array_combine(array_values($paramsKey), array_values($matches));
			$params['_controller'] = $route->getController();
			return $params;
		}

		/**
		* @deprecated Le controller est set dans match maintenant
		*/
		public function getController($id) {

			return $this->routes[$id]->getController();
		}
	}