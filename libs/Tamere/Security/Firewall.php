<?php

	namespace Tamere\Security;

	use Tamere\Http\Event\GetResponseEvent;

	class Firewall implements FirewallInterface {

		private $token;

		private $authenticators;

		private $templating;

		public function __construct(array $authenticators = array(), $templating){
			$this->authenticators = $authenticators;
			$this->templating = $templating;
		}

		public function handle(GetResponseEvent $event, $eventName, $router){
			$request = $event->getRequest();
			foreach($this->authenticators as $auth){
				$token = $auth->authenticate($request);
				if($token){
					$this->setToken($token);
					break;
				}
			}

		}

		private function setToken($token){
			$this->token = $token;
		}

		public function getToken(){
			return $this->token;
		}

		public function getObservedEvents() {
			return array("core.request" => "handle");
		}

		public function setAuthenticators(array $auth){
			$this->authenticators = $auth;
		}


	}