<?php
	namespace Tamere\Security;
	use Tamere\Event\EventObserverInterface;
	use Tamere\Http\Event\GetResponseEvent;

	interface FirewallInterface extends EventObserverInterface {

		public function handle(GetResponseEvent $event, $eventName, $router);
	}