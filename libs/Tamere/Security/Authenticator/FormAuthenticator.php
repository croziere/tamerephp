<?php
	namespace Tamere\Security\Authenticator;

	use Tamere\Http\Request;
	use Tamere\Security\Token\AuthenticatedToken;

	class FormAuthenticator implements AuthenticatorInterface {

		private $userProvider;

		public function __construct($repo){
			$this->userProvider = $repo;
		}

		public function authenticate(Request $request){
			if(!$request->request->has('_username') || !$request->request->has('_password')){
				return;
			}


			$user = $this->userProvider->findOneByUsername($request->request->get('_username'));
			if(!$user){
				throw new \Exception("Erreur de connexion");
			}
			$hash = hash('sha512', $request->request->get('_password'));
			if($user->getPassword() !== $hash){
				throw new \Exception("Erreur de connexion");	
			}

			$token = new AuthenticatedToken();
			$token->setUser($user);
			$token->setRoles($user->getRoles());
			$_SESSION['_id'] = $user->getId();
			return $token;
		}
	}