<?php
	namespace Tamere\Security\Authenticator;

	use Tamere\Http\Request;
	use Tamere\Security\Token\AuthenticatedToken;
	
	class SessionAuthenticator implements AuthenticatorInterface {

		private $userProvider;

		public function __construct($repo){
			$this->userProvider = $repo;
		}

		public function authenticate(Request $request){
			if(!$request->session->has('_id')){
				return;
			}

			$user = $this->userProvider->find($request->session->get('_id'));
			if(!$user){
				return;
			}
			$token = new AuthenticatedToken();
			$token->setUser($user);
			$token->setRoles($user->getRoles());
			return $token;
		}
	}