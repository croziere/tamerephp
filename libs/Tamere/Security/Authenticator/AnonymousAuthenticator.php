<?php
	namespace Tamere\Security\Authenticator;

	use Tamere\Http\Request;
	use Tamere\Security\Token\AnonymousToken;

	class AnonymousAuthenticator implements AuthenticatorInterface {

		public function authenticate(Request $request){
			return new AnonymousToken();
		}
	}