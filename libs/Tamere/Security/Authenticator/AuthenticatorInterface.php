<?php
	namespace Tamere\Security\Authenticator;

	use Tamere\Http\Request;

	interface AuthenticatorInterface {
		public function authenticate(Request $request);
	}