<?php
	namespace Tamere\Security\Authenticator;

	use Tamere\Http\Request;
	use Tamere\Security\Token\AnonymousToken;
	use Tamere\Http\ParametersBag;

	class LogoutAuthenticator implements AuthenticatorInterface {

		public function authenticate(Request $request){
			if($request->getPath() !== '/do-logout'){
				return;
			}
			$request->session = new ParametersBag();
			session_destroy();
			return;
		}
	}