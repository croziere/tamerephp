<?php
	namespace Tamere\Security\Token;

	abstract class Token {

		protected $user;

		protected $roles;

		public function setUser($user){
			$this->user = $user;
		}

		public function setRoles(array $roles){
			$this->roles = $roles;
		}

		public function getUser(){
			return $this->user;
		}

		public function getRoles(){
			return $this->roles;
		}

		public function isAuthenticated(){
			return isset($this->user) && isset($this->roles);
		}

	}