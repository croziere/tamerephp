<?php
	/**
	* Factory du vrai ClassLoader, register le ClassLoader
	*/
	class LibsAutoloader {

		private static $loader;

		/**
		* Inclus le vrai ClassLoader
		*/
		public static function registerClassLoader($class) {
			if('Autoload\ClassLoader' === $class) {
				require_once __DIR__.'/ClassLoader.php';
			}
		}

		public static function getLoader() {
			if(null !== self::$loader)
				return self::$loader;

			spl_autoload_register(array('LibsAutoloader', 'registerClassLoader'), true, true);
			self::$loader = new \Autoload\ClassLoader();
			spl_autoload_unregister(array('LibsAutoloader', 'registerClassLoader'));

			self::$loader->register(); //Enregistre le ClassLoader en tant qu'autoload

			$files = require __DIR__.'/required_files.php'; //Inclus les fichiers que le ClassLoader ne peut pas importer
			foreach($files as $file){
				requireFile($file);
			}

			return self::$loader;
		}

	}

function requireFile($file) {
	require $file;
}