<?php
	namespace Autoload;

	class ClassLoader {

		private $fileMap = array();

		public function addFile($namespace, $file) {
			$this->fileMap[$namespace] = __DIR__.'/../../'.$file;
		}

		public function loadClass($class) {			
			if(isset($this->fileMap[$class])) {

				includeFile($this->fileMap[$class]);
				return true; 
			}

			return $this->findFile($class);
		}

		public function register() {
			spl_autoload_register(array($this, 'loadClass'), true, true);
		}

		private function findFile($class) {
			$path = str_replace("\\", DIRECTORY_SEPARATOR, $class);
			$root = __DIR__.'/../../';
			$fileLibs = $root.'libs/'.$path.'.php';
			if(file_exists($fileLibs)){
				includeFile($fileLibs);
				return;
			}
			$fileModules = $root.'modules/'.$path.'.php';
			if(file_exists($fileModules)){
				includeFile($fileModules);
				return;
			}
		}
	}

function includeFile($file) {
	include $file;
}