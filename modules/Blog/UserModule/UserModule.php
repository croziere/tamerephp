<?php

	namespace Blog\UserModule;
	use Tamere\Http\Modules\Module;
	use Tamere\Routing\Route;
	
	class UserModule extends Module {
		
		public function boot(){
			$router = $this->container->get('router');

			$rootRoute = new Route('/login');
			$rootRoute->setController('Blog:User:User:login');
			$router->addRoute('user', $rootRoute);

			$logRoute = new Route('/do-login');
			$logRoute->setController('Blog:User:User:dologin');
			$router->addRoute('user.login', $logRoute);

			$outRoute = new Route('/do-logout');
			$outRoute->setController('Blog:User:User:dologout');
			$router->addRoute('user.logout', $outRoute);

			$signRoute = new Route('/sign-in');
			$signRoute->setController('Blog:User:User:signin');
			$router->addRoute('user.signin', $signRoute);
		}

	}
	
