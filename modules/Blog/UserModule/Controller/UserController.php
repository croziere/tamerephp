<?php
namespace Blog\UserModule\Controller;

use Tamere\Http\Response;
use Tamere\Http\Request;
use Tamere\DependencyInjection\ContainerAware;
use Blog\UserModule\Entity\User;

class UserController extends ContainerAware
{
	public function signinAction(Request $request) {
		if($this->container->get('firewall')->getToken()->isAuthenticated())
			throw new \Exception("Vous êtes déjà authentifié");
		$data = $request->request->all();
		if(isset($data['username']) && isset($data['password'])){
			$user = new User();
			$user->setUsername($data['username']);
			$user->setPassword(hash('sha512', $data['password']));
			$user->setLevel(1);
			$em = $this->container->get('entityManager');
			$em->persist($user);
			$em->flush();
			Header('Location:/login');
		}

		$templating = $this->container->get('templating');
		return $templating->render('signin.php', array('security' => $this->container->get('firewall')->getToken()));
	}

	public function loginAction() {
		if($this->container->get('firewall')->getToken()->isAuthenticated())
			throw new \Exception("Vous êtes déjà authentifié");

		$templating = $this->container->get('templating');
		return $templating->render('login.php', array('security' => $this->container->get('firewall')->getToken()));
	}

	public function dologinAction() {
		if($this->container->get('firewall')->getToken()->isAuthenticated())
			Header('Location:/');
		else
			Header('Location:/login');
	}

	public function dologoutAction() {
		Header('Location:/');
	}
}
