<?php

namespace Blog\UserModule\Entity;

/**
 * @Entity @Table(name="users")
 **/
class User {
	// Attributs
	
	/** @Id @Column(type="integer") @GeneratedValue **/
	private $id;
	/** @Column(type="string", length=50) **/
	private $username;
	/** @Column(type="integer") **/
	private $accessLevel;
	// Mot de passe encrypté
	/** @Column(type="string", length=255) **/
	private $password;
		
	// Constructeur
	/*
	 * @param int id
	 * @return void
	 * */	
	public function __construct() {
	}
	
	// Méthodes
	public function setId($id) {
		$this->id = $id;		
	}
	
	public function setUsername($name) {
		$this->username = $name;
	}
	
	public function setLevel($level) {
		$this->accessLevel = $level;
	}
	
	public function setPass($password) {
		$this->password = $password;
	}
	
	
	public function getId() {
		return $this->id;
	}
	
	public function getUsername() {
		return $this->username;
	}
	
	public function getLevel() {
		return $this->accessLevel;
	}

	public function getRoles(){
		return array($this->accessLevel);
	}
	
	public function getPass() {
		return $this->password;
	}

	public function getPassword(){
		return $this->password;
	}

	public function setPassword($password){
		$this->password = $password;
	}
}
