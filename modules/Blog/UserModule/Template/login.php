<?php $view->extend('layout.php');

$view['slots']->set('title', 'Connexion'); 

$view['slots']->set('headstyle', '<link href="css/special/blog.css" rel="stylesheet">');

?>

<form action="/do-login" method="post">
    <input type="text" placeholder="Login" name="_username">
    <input type="password" placeholder="Password" name="_password">
    <input type="submit" value="Connexion">
</form>
