<?php

namespace Blog\CommentaireModule\Entity;

/**
 * @Entity @Table(name="commentaires")
 **/
class Commentaire
{
	// Attributs
	/** @Id @Column(type="integer") @GeneratedValue **/
	private $id;
	/** @ManyToOne(targetEntity="Blog\UserModule\Entity\User") **/
	private $auteur;
	/** @Column(type="string", length=32) **/
	private $ip;
	/** @Column(type="datetime") **/
	private $postDate;
	/** @Column(type="text") **/
	private $contenu;	
	/** @ManyToOne(targetEntity="Blog\ArticleModule\Entity\Article", inversedBy="commentaires") **/
	private $articleSource;

	// Méthodes
	public function __construct() {
		$this->postDate = new \DateTime();
	}

	public function setArticle($article){
		$this->articleSource = $article;
	}

	public function getArticle(){
		return $this->articleSource;
	}
	
	public function setAuteur($auteur) {
		$this->auteur = $auteur;
	}
	
	public function getAuteur() {
		return $this->auteur;
	}
	
	public function setIp($ip) {
		$this->ip=$ip;
	}
	
	public function getIp() {
		$this->ip = $ip;
	}
	
	/*public function setPostDate($postDate) {
		$this->postDate = $postDate;
	}
	*/
	public function getPostDate() {
		return $this->postDate;
	}
	
	public function setContenu($content) {
		$this->contenu = $content;
	}
		
	public function getContenu() {
		return $this->contenu;
	}
	
	public function getId() {
		return $this->id;
	}
}
