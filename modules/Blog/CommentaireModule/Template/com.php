<?php $view->extend('layout.php');

$view['slots']->set('title', 'Ajouter un commentaire'); 

$view['slots']->set('headstyle', '<link href="/css/special/blog.css" rel="stylesheet">');

?>


<div class="alert alert-info" role="alert">Récapitulatifs de l'article</div>
	<div class="blog-post">
        <h2 class="blog-post-title"><?php echo($article->getTitre()); ?></h2>
        <p class="lead blog-description"><?php echo("#".$article->getId()." ".$article->getDescription()); ?></p>
        <p class="blog-post-meta">Posté le <?php echo(date('d/m/Y à H:i', $article->getPostDate()->getTimeStamp())." par ".$article->getAuteur()->getUsername()); ?></p>
        <p><?php echo $view->escape($article->getContenu()); ?></p>
        </p>
    </div>
<hr>
<div class="alert alert-info" role="alert">Commentaires liés à l'article</div>
<?php

	$commentaires = $article->getCommentaires();
	foreach($commentaires as $var)
	{
	?>
		<div class="blog-post">
		<h2 class="blog-post-title">De <?php echo $view->escape($var->getAuteur()->getUsername()); ?></h2
		<p class="blog-post-meta">Posté le <?php echo(date('d/m/Y à H:i', $var->getPostDate()->getTimeStamp()));?></p>
		<p><?php echo $view->escape($var->getContenu()); ?></p>
		</div>
	<?php
	}
?>
<br />
<hr>
<!-- TextArea + form !-->

<div class="alert alert-info" role="alert">Poster un commentaire</div>
<?php if($security->isAuthenticated()) { ?>
<form id="postComment" method="post" action="">
	<textarea name="comment_content">
	
	</textarea><br />
	<input type="submit" value="Poster"/>
</form>
<?php }else{ ?>
<div class="alert alert-warning" role="alert">Veuillez vous <a href="/login">connecter</a> pour poster des commentaires !</div>
<?php } ?>


