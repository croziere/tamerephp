<?php
namespace Blog\CommentaireModule\Controller;

use Tamere\Http\Response;
use Tamere\Http\Request;
use Tamere\DependencyInjection\ContainerAware;
use Blog\CommentaireModule\Entity\Commentaire;

class CommentaireController extends ContainerAware
{
	public function commentAction(Request $request, $id) {
		$em = $this->container->get('entityManager');
		$article = $em->getRepository('Blog\ArticleModule\Entity\Article')->find($id);
		if(!$article)
			throw new \Exception("Article inexistant");

		$data = $request->request->all();
		if(isset($data['comment_content'])){
			if($this->container->get('firewall')->getToken()->isAuthenticated()){
				$comm = new Commentaire();
				$comm->setArticle($article);
				$comm->setAuteur($this->container->get('firewall')->getToken()->getUser());
				$comm->setContenu($data['comment_content']);
				$comm->setIp('111');
				$em->persist($comm);
				$em->flush();
			}
		}
		
		$templating = $this->container->get('templating');
		return $templating->render('com.php', array('article'=>$article, 'security' => $this->container->get('firewall')->getToken()));
	}
}
