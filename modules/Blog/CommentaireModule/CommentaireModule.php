<?php

	namespace Blog\CommentaireModule;
	use Tamere\Http\Modules\Module;
	use Tamere\Routing\Route;
	
	class CommentaireModule extends Module {
		
		public function boot(){
			$router = $this->container->get('router');

			$rootRoute = new Route('/comment/:id', array('id' => '\d'));
			$rootRoute->setController('Blog:Commentaire:Commentaire:comment');
			$router->addRoute('comment', $rootRoute);
		}

	}
