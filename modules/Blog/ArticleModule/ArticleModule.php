<?php

	namespace Blog\ArticleModule;
	use Tamere\Http\Modules\Module;
	use Tamere\Routing\Route;
	
	class ArticleModule extends Module {
		
		public function boot(){
			$router = $this->container->get('router');

			$rootRoute = new Route('/');
			$rootRoute->setController('Blog:Article:Article:index');
			$router->addRoute('com', $rootRoute);

			$postRoute = new Route('/admin/post');
			$postRoute->setController('Blog:Article:Article:post');
			$router->addRoute('article.post', $postRoute);
		}

	}
	
