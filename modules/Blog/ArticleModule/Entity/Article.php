<?php

namespace Blog\ArticleModule\Entity;

/**
 * @Entity @Table(name="articles")
 **/
class Article
{
	// Attributs
	
	/** @Id @Column(type="integer") @GeneratedValue **/
	private $id;
	/** @Column(type="string", length=50) **/
	private $titre;
	/** @Column(type="string", length=60) **/
	private $description;
	/** @ManyToOne(targetEntity="Blog\UserModule\Entity\User") **/
	private $auteur;
	/** @Column(type="datetime") **/
	private $postDate;
	/** @Column(type="text") **/
	private $contenu;
	
	/** @OneToMany(targetEntity="Blog\CommentaireModule\Entity\Commentaire", mappedBy="articleSource") **/
	private $commentaires;

	
	// Méthodes
	public function __construct()
	{
		$this->postDate = new \DateTime();
		$this->commentaires = array();
	}

	public function setId($id) {
		$this->id = $id;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function setTitre($titre) {
		$this->titre = $titre;
	}
	
	public function getTitre() {
		return $this->titre;
	}
	
	public function setDescription($description) {
		$this->description = $description;
	}
	
	public function getDescription() {
		return $this->description;
	}
	
	
	public function setAuteur($auteur) {
		$this->auteur;
	}
	
	public function getAuteur() {
		return $this->auteur;
	}
	
	public function setPostDate($postDate) {
		$this->postDate = $postDate;
	}
	
	public function getPostDate() {
		return $this->postDate;
	}
	
	public function setContenu($contenu) {
		$this->contenu = $contenu;
	}
	
	public function getContenu() {
		return $this->contenu;
	}
	
	public function addCommentaire($commentaire) {
		$this->commentaires[] = $commentaire;
	}
	
	public function getCommentaires() {
		return $this->commentaires;
	}
	
}
