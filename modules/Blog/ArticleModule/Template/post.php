<?php $view->extend('layout.php');

$view['slots']->set('title', "Nouvel article"); 

$view['slots']->set('headstyle', '<link href="/css/special/blog.css" rel="stylesheet">');

?>

<form method="post" action="">
	<input name="titre" type="text" placeholder="Titre">
	<br>
	<input name="description" type="text" placeholder="Description">
	<br>
	<textarea name="content">
	</textarea>
	<br>
	<input type="submit" value="Ajouter l'article">
</form>
