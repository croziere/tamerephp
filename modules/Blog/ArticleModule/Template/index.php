<?php $view->extend('layout.php');

$view['slots']->set('title', 'Accueil : liste des news'); 

$view['slots']->set('headstyle', '<link href="css/special/blog.css" rel="stylesheet">');

?>

<?php foreach($articles as $var) { ?>
	<div class="blog-post">
        <h2 class="blog-post-title"><?php echo($var->getTitre()); ?></h2>
        <p class="lead blog-description"><?php echo("#".$var->getId()." ".$var->getDescription()); ?></p>
        <p class="blog-post-meta">Posté le <?php echo(date('d/m/Y à H:i', $var->getPostDate()->getTimeStamp())." par ".$var->getAuteur()->getUsername()); ?></p>
        <p><?php echo $var->getContenu(); ?></p>
        <p class="blog-post-meta">
        <?php 
			$total = count($var->Getcommentaires());
			echo("<a href=\"/comment/".$var->getId()."\"> ");
			if($total === 0)
				echo("Il n'y a aucun commentaire.");
			else if($total === 1)
				echo("Il y a 1 commentaire.");
			else
				echo("Il y a ".$total." commentaires.");		
			echo "</a>";
        ?>
        </p>
    </div>

<?php } ?>

<nav>
	<ul class="pager">
		<li><a href="#">Précédent</a></li>
		<li><a href="#">Suivant</a></li>
	</ul>
</nav>
