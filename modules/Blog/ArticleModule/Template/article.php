<?php $view->extend('layout.php');

$view['slots']->set('title', $article->getTitre()); 

$view['slots']->set('headstyle', '<link href="css/special/blog.css" rel="stylesheet">');

?>

<div class="blog-post">
    <h2 class="blog-post-title"><?php echo($article->getTitre()); ?></h2>
       <p class="lead blog-description"><?php echo("#".$article->getId()." ".$article->getDescription()); ?></p>
       <p class="blog-post-meta">Posté le <?php echo(date('d/m/Y à H:i', $article->getPostDate()->getTimeStamp())." par ".$article->getAuteur()->getUsername()); ?></p>
        <p><?php echo $article->getContenu(); ?></p>
    </div>
