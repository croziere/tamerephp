<?php
namespace Blog\ArticleModule\Controller;

use Tamere\Http\Response;
use Tamere\Http\Request;
use Tamere\DependencyInjection\ContainerAware;
use Blog\ArticleModule\Entity\Article;

class ArticleController extends ContainerAware
{
	public function indexAction() {
		
		$em = $this->container->get('entityManager');
		$articles = $em->getRepository('Blog\ArticleModule\Entity\Article')->findAll();
		
		$templating = $this->container->get('templating');
		return $templating->render('index.php', array('articles'=>$articles, 'security' => $this->container->get('firewall')->getToken()));
	}

	public function postAction(Request $request){
		$token = $this->container->get('firewall')->getToken();
		if(!$token->isAuthenticated() || !in_array(2, $token->getRoles()))
			throw new \Exception('Accès interdit');

		$data = $request->request->all();

		if(isset($data['titre']) && isset($data['content']) && isset($data['description'])){
			$art = new Article();
			$art->setAuteur($token->getUser());
			$art->setDescription($data['description']);
			$art->setTitre($data['titre']);
			$art->setContenu($data['content']);
			$em = $this->container->get('entityManager');
			$em->persist($art);
			$em->flush();
		}

		$tpl = $this->container->get('templating');
		return $tpl->render('post.php', array('security' => $this->container->get('firewall')->getToken()));
	}
}
