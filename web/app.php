<?php
	error_reporting(E_ALL);
	date_default_timezone_set('Europe/Paris');
	ini_set('display_errors', 1);
	use Tamere\Http\Request;
	use App\ApplicationKernel;
	
	require_once __DIR__.'/../app/bootstrap.php.cache';
	$kernel = new ApplicationKernel('dev', true);
	$request = Request::createUserRequest();
	$response = $kernel->handle($request);
	$response->send();
?>
