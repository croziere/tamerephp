<?php
	require __DIR__.'/../vendor/autoload.php';
	$loader = require __DIR__.'/../libs/autoload.php';
	$loader->addFile('App\ApplicationKernel', 'app/ApplicationKernel.php');
	return $loader;