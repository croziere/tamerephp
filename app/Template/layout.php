<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    
    <link rel="icon" href="favicon.ico">

    <title><?php $view['slots']->output('title', 'News-Meric'); ?></title>

    <!-- Bootstrap core CSS -->
    <link href="/css/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <?php $view['slots']->output('headstyle'); ?>
    
  </head>

  <body>

    <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item active" href="/">Accueil</a>
          <?php if($security->isAuthenticated()) { 
            if(in_array(2, $security->getRoles())){ ?>
              <a class="blog-nav-item active" href="/admin/post">Ajouter un article</a>
        <?php } ?>
            <a class="blog-nav-item active" href="/do-logout">Déconnexion (<?php echo $view->escape($security->getUser()->getUsername()) ?>)</a>
          <?php }else{ ?>
            <a class="blog-nav-item active" href="/login">Se connecter</a>
            <a class="blog-nav-item active" href="/sign-in">S'inscrire</a>
          <?php } ?>
        </nav>
      </div>
    </div>

    <div class="container">

      <div class="row">

        <div class="col-sm-8 blog-main">
		
		<?php $view['slots']->output('_content'); ?>
		
        </div><!-- /.blog-main -->

        <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
          <div class="sidebar-module sidebar-module-inset">
            <h4>A propos</h4>
            <p>IUT Informatique Clermont-Ferrand</p>
          </div>
          
          <div class="sidebar-module">
            <h4>Liens</h4>
            <ol class="list-unstyled">
              <li><a href="#">lien test</a></li>
            </ol>
          </div>
        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->

    <footer class="blog-footer">
      <p>Blog template built for <a href="http://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
