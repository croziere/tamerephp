<?php
	namespace App;

	use Tamere\Application\AbstractApplicationKernel;


	class ApplicationKernel extends AbstractApplicationKernel {

		public function registerModules() {
			$modules = array(
				new \Tamere\Modules\ConfigModule\ConfigModule(),
				new \Tamere\Modules\DoctrineModule\DoctrineModule(),
				new \Blog\ArticleModule\ArticleModule(),
				new \Blog\UserModule\UserModule(),
				new \Blog\CommentaireModule\CommentaireModule(),
				);

			return $modules;
		}

	}
